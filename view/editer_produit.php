<?php
require_once dirname(__DIR__)."/controlleur/ProduitController.php";
require_once dirname(__DIR__)."/model/produit.php";
$controller = new ProduitController();
$id = filter_var($_SERVER["PHP_SELF"],FILTER_SANITIZE_NUMBER_INT);

$produit = $controller::$RepoProduit->rechercherProduit((int)$id);



if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $designation = htmlspecialchars(trim(stripslashes($_POST["designation"])));
	$prix = htmlspecialchars(trim(stripslashes($_POST["prix"])));
    $clean = true;
    if (isset($_POST["id"])){
	$id = htmlspecialchars(trim(stripslashes($_POST["id"])));
    if (empty($id)){
		$clean = false;
		$empty_id = "entrer l'id";
	}
    if(!empty($id)&&!preg_match("/^\d+$/",$id)){
        $error_id = "id doit etre un nombre";
        $clean = false;
    }
    }
    if (empty($designation)){
		$clean = false;
		$empty_designation = "entrer la designation";
	}
	if (empty($prix)){
		$clean = false;
		$empty_prix = "entre le prix unitaire";
	}
    if(!empty($designation)&&strlen($designation)>20){
        $error_designation  = "la designation doit etre moin de 20 caractere";
        $clean = false;		
}
    if(!empty($prix)&&!preg_match("/^\d+\.?\d+$/",$prix)){
        $error_prix = "le prix pas valide";
        $clean = false;
    }
    if ($clean){
        if( $produit == false){
            $msg = "pas de Produit ayant cet id";
        }
        else{
            $replaced_obj = new Produit((int) $id,$designation,(float)$prix);
            $controller::$RepoProduit->modifierProduit((int) $id,$replaced_obj);
            $success = "le produit ayant l'id $id est modifier avec success";
        }
        
    }
}

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style>
		form {
			width: 400px;
			margin: auto;
			margin-top: 100px;
			border: 2px double black;
			height: 500px;
			display: flex;
			flex-direction: column;
		}

		form>div {
			width: 100%;
			margin: 25px 0;
			display: flex;
			justify-content: space-around;
		}

		div label {
			width: 100px;
		}
		div>input[type="submit"]{
			width: 100px;
			height: 35px;
			border-radius: 10px;
			border: 2px ;
			font-size: 1.1rem;
			cursor: pointer;
			background-color:aquamarine;
			margin-top: 100px;
		}.error{
			text-align: center;
			color: red;
		}
        .success{
            color: green;
            text-align: center;
        }
	</style>
</head>

<body>
    <h2 class="success"><?=isset($success)?$success:""?></h2>
	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
	<h3 style="text-align:center;">Modifier Produit</h3>
		<div>
			<label for="">Id</label>
			<?=(empty($id))?"<input type='text'  name='id'>":"<label>$id<label>"?>
		</div>
		<span class="error"><?=isset($empty_id)?$empty_id:""?></span>
		<span class="error"><?=isset($error_id)?$error_id:""?></span>
		<span class="error"><?=isset($id_exists)?$id_exists:""?></span>
		<div>
			<label for="">Designation</label>
			<input type="text" value="<?php 
            if(($produit !==false)){
                echo $produit->getDesignation();
            }
            else {
                echo '';
            }
            ?>" name="designation">
		</div>
		<span class="error"><?=isset($empty_designation)?$empty_designation:""?></span>
		<span class="error"><?=isset($error_designation)?$error_designation:""?></span>		<div>
			<label for="">Prix unitaire</label>
			<input type="text" value="<?php 
            if(($produit !==false)){
                echo $produit->getPrixUnitaire();
            }
            else {
                echo '';
            }
            ?>" name="prix">
		</div>
		<span class="error"><?=isset($empty_prix)?$empty_prix:""?></span>
		<span class="error"><?=isset($error_prix)?$error_prix:""?></span>		<div>
			<input type="submit" name="modifier" value="modifier">
		</div>
	</form>

</body>

</html>