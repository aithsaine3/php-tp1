<?php
require_once dirname(__DIR__)."/model/produit.php";
require_once dirname(__DIR__)."/controlleur/ProduitController.php";
$controller =new ProduitController();
if ($_SERVER["REQUEST_METHOD"]=="POST"){

	$id = htmlspecialchars(trim(stripslashes($_POST["id"])));
	$designation = htmlspecialchars(trim(stripslashes($_POST["designation"])));
	$prix = htmlspecialchars(trim(stripslashes($_POST["prix"])));
	$clean = true;
	if (empty($id)){
		$clean = false;
		$empty_id = "entrer l'id";
	}
	if (empty($designation)){
		$clean = false;
		$empty_designation = "entrer la designation";
	}
	if (empty($prix)){
		$clean = false;
		$empty_prix = "entre le prix unitaire";
	}
	if(!empty($id)&&!preg_match("/^\d+$/",$id)){
			$error_id = "id doit etre un nombre";
			$clean = false;
	}
	if(!empty($designation)&&strlen($designation)>20){
			$error_designation  = "la designation doit etre moin de 20 caractere";
			$clean = false;		
	}
	if(!empty($prix)&&!preg_match("/^\d+\.?\d+$/",$prix)){
			$error_prix = "le prix pas valide";
			$clean = false;
	}

if ($clean){
	if ($controller::$RepoProduit->rechercherProduit((int) $id) !== false){
		$id_exists = "id deja existe avec un autre produit";
	}
	else{
		$new_produit = new Produit((int) $id,$designation,(float) $prix);
		 $controller::$RepoProduit->ajouterProduit($new_produit);
			$success = "alert('le nouveau produit est enregistrer avec success')";
		
	}
}
}

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style>
		form {
			width: 400px;
			margin: auto;
			margin-top: 100px;
			border: 2px double black;
			height: 500px;
			display: flex;
			flex-direction: column;
		}

		form>div {
			width: 100%;
			margin: 25px 0;
			display: flex;
			justify-content: space-around;
		}

		div label {
			width: 100px;
		}
		div>input[type="submit"]{
			width: 100px;
			height: 35px;
			border-radius: 10px;
			border: 2px ;
			font-size: 1.1rem;
			cursor: pointer;
			background-color:aquamarine;
			margin-top: 100px;
		}.error{
			text-align: center;
			color: red;
		}
	</style>
</head>

<body>
	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
	<h3 style="text-align:center;">Enregistrer un nouveau Produit</h3>
		<div>
			<label for="">Id</label>
			<input type="text" value="<?=isset($id)?$id:""?>" name="id">
		</div>
		<span class="error"><?=isset($empty_id)?$empty_id:""?></span>
		<span class="error"><?=isset($error_id)?$error_id:""?></span>
		<span class="error"><?=isset($id_exists)?$id_exists:""?></span>
		<div>
			<label for="">Designation</label>
			<input type="text" value="<?=isset($designation)?$designation:""?>" name="designation">
		</div>
		<span class="error"><?=isset($empty_designation)?$empty_designation:""?></span>
		<span class="error"><?=isset($error_designation)?$error_designation:""?></span>		<div>
			<label for="">Prix unitaire</label>
			<input type="text" value="<?=isset($prix)?$prix:''?>" name="prix">
		</div>
		<span class="error"><?=isset($empty_prix)?$empty_prix:""?></span>
		<span class="error"><?=isset($error_prix)?$error_prix:""?></span>		<div>
			<input type="submit" name="envoyer" value="enregistrer">
		</div>
	</form>
<script>
	<?=isset($success)?$success:""?>
</script>
</body>

</html>