<?php
require_once dirname(__DIR__)."/repositorie/RepoProduit.php";
class ProduitController{
    public static RepoProduit $RepoProduit;

    public function __construct()
    {
        self::$RepoProduit = new RepoProduit();        
    }
    public function index(){
        $produits = self::$RepoProduit->getTousProduit();
        $html = "<table id='customers'>";
        $html .= "<tr>";
        $html .= "<th>id</th>";
        $html .= "<th>designation</th>";
        $html .= "<th>prix unitaire</th>";
        $html .= "</tr>";
        foreach ($produits as $produit){
            $html .= "<tr>";
            $html .= "<td>".$produit->getId()."</td>";
            $html .= "<td>".$produit->getDesignation()."</td>";
            $html .= "<td>".$produit->getPrixUnitaire()."</td>";
            $html .= "</tr>";
        }
        
        $html .= "</table>";
        echo $html;
    }
    
}