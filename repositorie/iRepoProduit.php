<?php

interface iRepoProduit{
    function ajouterProduit(Produit $produit):void;
    function modifierProduit(int $id,Produit $produit):void;
    function supprimerProduit(int $id):void;
    function rechercherProduit(int $id):Produit|false;
    function getTousProduit():array;
}