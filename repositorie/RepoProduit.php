<?php
require_once __DIR__."/iRepoProduit.php";
require_once dirname(__DIR__)."/model/produit.php";
class RepoProduit implements iRepoProduit{
    public static $pdo;

    public function __construct()
    {
        self::$pdo = new PDO("mysql:host=localhost;port=3306;dbname=produits","root","53267",[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    function ajouterProduit(Produit $produit):void{
        self::$pdo->prepare("insert into produit(id, designation, prix_unitaire) values
        (?,?,?)")->execute([$produit->getId(),$produit->getDesignation(),$produit->getPrixUnitaire()]);
        
    }
    function modifierProduit(int $id,Produit $produit):void{
        self::$pdo->prepare("update produit set designation = ?,prix_unitaire = ? where id=? ;")->execute([$produit->getDesignation(),$produit->getPrixUnitaire(),$produit->getId()]);

    }
    function supprimerProduit(int $id):void{
        self::$pdo->prepare("delete from produit where id = ?")->execute([$id]);

    }
    function rechercherProduit(int $id):Produit|false{
        $stm = self::$pdo->query("select * from produit where id =$id");
        $data = $stm->fetch();
        if ($data){
            return new Produit((int)$data["id"],$data["designation"],(float) $data["prix_unitaire"]);
        }
        else{
            return false;
        }
    }
    function getTousProduit():array{
        $produits = [];
        $stm = self::$pdo->query("select * from produit");
        $prds = $stm->fetchAll();
        if ($prds>0){
            foreach($prds as $item){
                $produits[]= new Produit((int)$item["id"],$item["designation"],(float) $item["prix_unitaire"]);
            }
        }
        return $produits;

    }
}

/* $pr1 = new Produit(1, "laptop", 11000);
$pr2 = new Produit(2, "choese", 90);
$pr3 = new Produit(3,"printer", 600);

$repoproduit = new RepoProduit();
 $repoproduit->ajouterProduit($pr1);
$repoproduit->ajouterProduit($pr2);
$repoproduit->ajouterProduit($pr3);

$data = $repoproduit->getTousProduit();
$item = $repoproduit->rechercherProduit(1);

echo "<pre>";
print_r($item);
 */