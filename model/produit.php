<?php

class Produit{
    
    private int $id;
    private string $designation;
    private float $prix_unitaire;
    
    public function __construct(int $id, string $designation, float $prix_unitaire)
    {
        $this->id = $id;
        $this->designation = $designation;
        $this->prix_unitaire = $prix_unitaire;
    }
    public function getId(){
        return $this->id;
    }
    public function getDesignation(){
        return $this->designation;
    }
    public function setDesignation(string $new_designation):void{
        $this->designation = $new_designation;
    }
    public function getPrixUnitaire():float{
        return $this->prix_unitaire;
    }
    public function setPrixUnitaire(float $new_prix):void{
        $this->prix_unitaire = $new_prix;
    }
}